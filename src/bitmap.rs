/*
 * Copyright 2018 Rodrigo R. M. Kochenburger <rk@divoxx.me>
 * Author: Rodrigo R. M. Kochenburger <rk@divoxx.me>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

use log::trace;
use std::mem::size_of;

static BITS_IN_WORD: usize = size_of::<usize>() * 8;

#[derive(Debug)]
pub struct Bitmap {
    size:    usize,
    buckets: Vec<usize>,
}

impl Bitmap {
    pub fn new(size: usize) -> Bitmap {
        let buckets_count = Self::calculate_position(size).0 + 1;
        let buckets = vec![0; buckets_count];

        trace!(
            "Bitmap initialization data: size={}, buckets_count={}",
            size,
            buckets_count
        );

        Bitmap { size, buckets }
    }

    pub fn set(&mut self, i: usize) {
        assert!(i < self.size);
        let (bucket, offset) = Self::calculate_position(i);
        self.buckets[bucket] |= 1 << offset;
    }

    pub fn unset(&mut self, i: usize) {
        assert!(i < self.size);
        let (bucket, offset) = Self::calculate_position(i);
        self.buckets[bucket] ^= 1 << offset;
    }

    pub fn get(&self, i: usize) -> bool {
        assert!(i < self.size);
        let (bucket, offset) = Self::calculate_position(i);
        self.buckets[bucket] & 1 << offset > 0
    }

    fn calculate_position(i: usize) -> (usize, usize) {
        let bucket_offset = i % BITS_IN_WORD;
        let bucket_number = i / BITS_IN_WORD;

        trace!(
            "calculated position using word size of {} bits: ({}, {}) for {}",
            BITS_IN_WORD,
            bucket_number,
            bucket_offset,
            i
        );

        (bucket_number, bucket_offset)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::tests::setup_logger;

    #[test]
    fn it_sets_and_gets_correctly() {
        setup_logger();
        let mut bitset = Bitmap::new(65);

        let test_case = [0, 1, 31, 32, 64];

        for i in test_case.iter() {
            bitset.set(*i);
        }

        for i in 0..65 {
            if test_case.contains(&i) {
                assert!(bitset.get(i));
            } else {
                assert!(!bitset.get(i));
            }
        }
    }

    #[test]
    fn it_correctly_unsets_bits() {
        setup_logger();
        let mut bitset = Bitmap::new(8);

        bitset.set(1);
        bitset.set(2);
        bitset.set(3);
        bitset.unset(2);

        assert!(bitset.get(1));
        assert!(!bitset.get(2));
        assert!(bitset.get(3));
    }
}
