/*
 * Copyright 2018 Rodrigo R. M. Kochenburger <rk@divoxx.me>
 * Author: Rodrigo R. M. Kochenburger <rk@divoxx.me>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pub trait Stack<I> {
    fn push(&mut self, item: I);
    fn pop(&mut self) -> Option<I>;
}

pub struct VecStack<I: Sized> {
    vec: Vec<I>,
}

impl<I> VecStack<I> {
    pub fn new() -> VecStack<I> {
        VecStack {
            vec: Vec::<I>::new(),
        }
    }
}

impl<I> Stack<I> for VecStack<I> {
    fn push(&mut self, item: I) {
        self.vec.push(item);
    }

    fn pop(&mut self) -> Option<I> {
        self.vec.pop()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::tests::setup_logger;

    #[test]
    fn it_push_and_pop_items() {
        setup_logger();

        let mut s = VecStack::new();

        s.push(1);
        s.push(1337);

        assert_eq!(Some(1337), s.pop());
        assert_eq!(Some(1), s.pop());
        assert_eq!(None, s.pop());
    }
}
