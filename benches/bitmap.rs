use awds::bitmap::Bitmap;
use criterion::{criterion_group, criterion_main, Bencher, Criterion};

fn bench_bitmap_set(c: &mut Criterion) {
    let mut bm = Bitmap::new(64);

    c.bench_function_over_inputs(
        "set",
        move |b: &mut Bencher, i: &usize| {
            b.iter(|| bm.set(*i));
        },
        vec![0, 1, 32, 63],
    );
}

fn bench_bitmap_get(c: &mut Criterion) {
    let mut bm = Bitmap::new(64);
    bm.set(0);
    bm.set(63);

    c.bench_function_over_inputs(
        "get",
        move |b: &mut Bencher, i: &usize| {
            b.iter(|| bm.get(*i));
        },
        vec![0, 1, 32, 63],
    );
}

criterion_group!(benches, bench_bitmap_set, bench_bitmap_get);
criterion_main!(benches);
